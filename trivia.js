/* eslint-disable linebreak-style */
/* eslint-disable guard-for-in */
/* eslint-disable max-len */
const axios = require('axios').default;
const fs = require('fs');
const {program} = require('commander');
// eslint-disable-next-line no-unused-vars
const colors = require('colors');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

// These are the global variables
let play = true; // Boolean to choose to play or see the scores
let successes = 0; // Number of successes
let fails = 0; // Number of fails
const userAnswers = []; // Stores the user's answers
const correctAnswers = []; // Store the correct answers
const possibleAnswers = []; // Store the amount of possible answers of every question (2 if True/false, 4 if multiple choice)
let currentIndex = 0;

/**
 * Evaluate if the mandatory option entered is correct
 * @param {string} value
 */
function playOrScores(value) {
  if (value == 'p') {
    console.log('Let\'s play Trivia Game');
  } else if (value == 's') {
    console.log('Let\'s see previous scores');
    play = false;
  } else {
    console.error('You have to pass one argument: p to play, or s to list scores');
    process.exit(1);
  }
}


program
    .requiredOption('-o, --option-game <string>', 'mandatory param, p for play, s to see scores', playOrScores)
    .option('-n, --number <number>', 'optional param to select the number of questions', '10');

program.parse(process.argv);

/**
 * @param {object} item The complete JSON quetion with category, type, difficulty,...
 * Shows on the console the question, and all the answers
 */
const displayQuestion = (item) => {
  const totalAnswers = item.incorrect_answers;
  totalAnswers.push(item.correct_answer);
  totalAnswers.sort();
  possibleAnswers.push(totalAnswers.length);
  correctAnswers.push(totalAnswers.indexOf(item.correct_answer));
  process.stdout.write(`${entities.decode(item.question)}\n`.red.bold);
  for (index in totalAnswers) {
    process.stdout.write(`${index}. ${entities.decode(totalAnswers[index])}\n`.cyan);
  }
};

/**
 * In case we selected to play the trivia game ('-o p' param)
 */
if (play) {
  // With axios, we do a GET request to the API url
  axios({
    method: 'get',
    url: 'https://opentdb.com/api.php?amount='+program.number,
    responseType: 'json',
    responseEncoding: 'utf8',
  })
      .then(function(response) {
        const questions = response.data.results; // Stores the questions data (question, answers, category,...)

        let userAnswer;
        displayQuestion(questions[0]); // Shows on the console the first question
        // The process.openStdin().on allows the user to write on the console
        process.openStdin().on('data', function(d) {
          userAnswer = d.toString().trim();
          if (userAnswer.length === 1 && (parseInt(userAnswer) >= 0 || parseInt(userAnswer) < possibleAnswers[currentIndex])) {
            userAnswers.push(userAnswer); // Stores the answers the user is giving
            if (userAnswers.length < questions.length) {
              currentIndex++;
              displayQuestion(questions[userAnswers.length]); // Continue displaying the rest of the answers
            } else {
              // When the game is finished we add the score to the file with all the historic scores
              for (index in userAnswers) {
                if (userAnswers[index] == correctAnswers[index]) { // Compare the answers given with the correct answers
                  successes++;
                } else {
                  fails++;
                }
              }
              const percentage = (successes * 100 / (successes + fails)).toFixed(2);
              const time = new Date().toISOString().replace(/\..+/, ''); // We get the date when the trivia is finished
              const newScore = time + '   ' + percentage.toString() +'%\n';
              fs.appendFile('scoreList.txt', newScore, (err) => { // Add a new line with the current score and time to the file with the scores
                if (err) {
                  console.log('There was an error appending in the file!');
                } else {
                  process.stdout.write('You have finished the trivia and your score has been added\n'.brightGreen);
                  process.exit();
                }
              });
            }
          } else {
            console.log(`Your answer must be an integer between 0 and ${possibleAnswers[currentIndex]}`.magenta.underline);
          }
        });
      });
}


/**
 * In case we selected to see the previous scores ('-o s' param)
 */
if (!play) {
  fs.readFile('scoreList.txt', (err, data) => {
    if (err) {
      console.log('There was an error reading the file!');
    } else {
      console.log(String(data));
    }
  });
}


