You must pass a mandatory param to use the API. You have 2 options
- -o p // --option p -> To play the game (You can also use npm start)
- -o s // --option s -> To list the scores

You can also add another param: -n // --number
This param set the number of questions. By default is 10.